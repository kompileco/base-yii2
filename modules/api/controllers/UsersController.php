<?php

namespace app\modules\api\controllers;

use yii;
use yii\web\Controller;
use yii\web\UnauthorizedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\MethodNotAllowedHttpException;

/**
 * Default controller for the `api` module
 */
class UsersController extends Controller {

    public function actionGet() {
        $id = Yii::$app->request->post('identification');

        if ($id)
            $model = \app\models\Users::find()->select(['id_users', 'identification_users', 'name', 'surname', 'telephone', 'address', 'email', 'id_city'])->where(['identification_users' => $id])->asArray()->one();
        else
            $model = \app\models\Users::find()->select(['id_users', 'identification_users', 'name', 'surname', 'telephone', 'address', 'email', 'id_city'])->asArray()->all();

        if (!$model)
            $response = [
                'status' => 'error',
                'message' => 'No existe usuario',
                    //'data' => $model
            ];
        else
            $response = [
                'status' => 'ok',
                'message' => '',
                'data' => $model
            ];

        return $response;
    }

    public function actionCreate() {
        $identification = Yii::$app->request->post('identification');

        $model = new \app\models\Users();
        $model->setAttributes(Yii::$app->request->post());
        $model->identification_users = $identification;
        
        if (!$model->save()) {
            $error = '';
            foreach ($model->getFirstErrors() as $key => $value) {
                $error = "$key: $value";
                break;
            }
            $response = [
                'status' => 'error',
                'message' => $error,
            ];
        } else
            $response = [
                'status' => 'ok',
                'message' => '',
                'data' => ['id_users' => $model->id_users]
            ];

        return $response;
    }

    Public function beforeAction($action) {

        $this->enableCsrfValidation = false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $pass = (cryptPassword(Yii::$app->params['api']));
        $token = Yii::$app->request->headers->get('token');

        if ($token != $pass)
            Throw new UnauthorizedHttpException('Acceso denegado');
        if (!Yii::$app->request->isPost)
            Throw new MethodNotAllowedHttpException('Petición inválida');

        return parent::beforeAction($action);
    }

}
