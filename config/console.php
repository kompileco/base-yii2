<?php

require_once(__DIR__ . '/functions.php');

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

defined('YII_ENV') or define('YII_ENV', 'console');

$config = [
    'id' => 'basic-console',
    'language' => 'es', // Set the language here
    'timeZone' => 'America/Bogota',
    'name' => 'MiDependiente',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mx1.hostinger.co',
                'username' => $params['infoEmail'],
                'password' => $params['infoEmailPass'],
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
        'urlManager' => [
            'baseUrl' => 'https://usuarios.midependiente.com',
            'hostInfo' => 'https://usuarios.midependiente.com',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            //'suffix'          => '/', 
            'rules' => [
                '<alias:\w+>' => 'site/<alias>',
                //'<web\w+>\\'=>'<web>/index',
                '<controller:\w+>\\' => '<controller>/index',
                '<controller:\w+>/<id:\d+>' => '<controller>/ver',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
//                '<controller:\w+>/<action:\w+>/actualizar/<id:\d+>' => '<controller>/<action>/<actualizar>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
        'Festivos' => [
            'class' => 'app\components\Festivos',
        ],
    ],
    'params' => $params,
        /*
          'controllerMap' => [
          'fixture' => [ // Fixture generation command line.
          'class' => 'yii\faker\FixtureController',
          ],
          ],
         */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
