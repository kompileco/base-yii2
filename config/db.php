<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=' . $params['db_host'] . ';dbname=' . $params['db_schema'],
    'username' => $params['db_user'],
    'password' => $params['db_pass'],
    'charset' => 'utf8',
    'on afterOpen' => function($event) {
        try{
            $event->sender->createCommand("SET time_zone = 'America/Bogota'")->execute();
        }catch(Exception $e){
           // dd($e);
        }
        
    },
     
];
