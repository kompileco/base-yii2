<?php

use kartik\mpdf\Pdf;

/* Include debug functions */
require_once(__DIR__ . '/functions.php');

$params = require(__DIR__ . '/params.php');


$config = [
    'params' => $params,
    'id' => 'basic',
    'language' => $params['lang'], // Set the language here
    'timeZone' => $params['timeZone'],
    'name' => $params['appName'],
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'assetManager' => [
            'appendTimestamp' => true, // here's the magic
        ],
        // setup Krajee Pdf component
        'pdf' => [
            'class' => Pdf::classname(),
            'cssFile' => "@webroot/css/pdf.css",
            'format' => Pdf::FORMAT_LEGAL,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'marginLeft' => 5,
            'marginTop' => 15,
            'marginRight' => 5,
            'marginBottom' => 15,
            'marginHeader' => 5,
            'marginFooter' => 5,
        // refer settings section for all configuration options
        ],
        'view' => [
            'class' => '\rmrevin\yii\minify\View',
            'enableMinify' => YII_ENV_PROD,
            'concatCss' => true, // concatenate css
            'minifyCss' => true, // minificate css
            'concatJs' => true, // concatenate js
            'minifyJs' => true, // minificate js
            'minifyOutput' => true, // minificate result html page
            'webPath' => '@web', // path alias to web base
            'basePath' => '@webroot', // path alias to web base
            'minifyPath' => '@webroot/minify', // path alias to save minify result
            'jsPosition' => [\yii\web\View::POS_END], // positions of js files to be minified
            'forceCharset' => 'UTF-8', // charset forcibly assign, otherwise will use all of the files found charset
            'expandImports' => true, // whether to change @import on content
            'compressOptions' => ['extra' => true], // options for compress
            'excludeFiles' => [
//	'jquery.js', // exclude this file from minification
//'app-[ ^.].js ', // you may use regexp
            ],
            'excludeBundles' => [
//\app\helloworld\AssetBundle::class, // exclude this bundle from minification
            ],
        ],
        'request' => [
// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'nXy3ZpW9MKo7Ywj5i9hxpxILUn3JirVf',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\Users',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => $params['hostEmail'],
                'username' => $params['infoEmail'],
                'password' => $params['infoEmailPass'],
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            //'suffix'          => '/', 
            'rules' => [
               '<alias:\w+>' => 'site/<alias>',
                //'<web\w+>\\'=>'<web>/index',
                '<controller:\w+>\\' => '<controller>/index',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                 
                //['class' => 'yii\rest\UrlRule', 'controller' => ['api/users', 'api/services']],
            ],
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'thousandSeparator' => '.',
            'decimalSeparator' => ', ',
            'locale' => 'es-CO',
            'currencyCode' => 'COP',
            'numberFormatterSymbols' => [
                NumberFormatter::CURRENCY_SYMBOL => 'USD $ ',
            ]
        ],
        'Festivos' => [
            'class' => 'app\components\Festivos',
        ],
    ],
    'modules' => [
        'api' => [
            //'basePath' => '@app/modules/api',
            'class' => 'app\modules\api\Module',
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module'
        ]
    ],
    'aliases' =>
    [
        '@images' => '@app/uploads/images',
        '@operativos' => '@app/uploads/operativos',
        '@firma' => '@app/uploads/firmas',
        '@logo' => '@app/uploads/logos',
        '@pdf' => '@app/pdf',
       
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
            // uncomment the following to add your IP if you are not connecting from localhost.
            //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [//here
            'crud' => [// generator name
                'class' => 'yii\gii\generators\crud\Generator', // generator class
                'templates' => [//setting for out templates
                    'adminLte' => '@app/views/layouts/crud', // template name => path to template
                ]
            ]
        ],
            // uncomment the following to add your IP if you are not connecting from localhost.
            //'allowedIPs' => ['127.0.0.1', '::1   '],
    ];
}

return $config;
