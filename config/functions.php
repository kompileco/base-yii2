<?php

use kartik\widgets\Alert;

const STATUS_INACTIVO = 0;
const STATUS_ACTIVO = 1;

/**
 * Debug function
 * d($var);
 */
function d($var, $caller = null) {
    if (!isset($caller)) {
        $aux = debug_backtrace(1);
        $caller = array_shift($aux);
    }
    echo '<code>File: ' . $caller['file'] . ' / Line: ' . $caller['line'] . '</code>';
    echo '<pre>';
    yii\helpers\VarDumper::dump($var, 10, true);
    echo '</pre>';
}

/**
 * Debug function with die() after
 * dd($var);
 */
function dd($var) {
    $aux = debug_backtrace(1);
    $caller = array_shift($aux);
    d($var, $caller);
    die();
}

function showFlash() {
    $time = 10000;
    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
        $type = Alert::TYPE_DEFAULT;
        switch ($key) {
            case 'success':
                $type = Alert::TYPE_SUCCESS;
                break;
            case 'warning':
                $type = Alert::TYPE_WARNING;
                break;
            case 'danger':
                $type = Alert::TYPE_DANGER;
                break;
        }

        echo Alert::widget([
            'type' => $type,
            'body' => $message,
            'delay' => $time
        ]);
    }
}

function showSeparator() {
    return"<div class='form-group'>
                <div class='col-md-offset-2 col-md-10'>
                    <hr>                    
                </div>
            </div>";
}

function showIconStatus($value) {
    if ($value) {
        return \Yii::$app->params['iconActivo'];
    } else {
        return \Yii::$app->params['iconInactivo'];
    }
}

function hoy($format = 'Y-m-d') {
    return date($format);
}

function platformSlashes($path) {
    if (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') {
        $path = str_replace('/', '\\', $path);
    }
    return $path;
}

function deleteDir($dirname) {
    if (is_dir($dirname)) {
        $dir = new RecursiveDirectoryIterator($dirname, RecursiveDirectoryIterator::SKIP_DOTS);
        foreach (new RecursiveIteratorIterator($dir, RecursiveIteratorIterator::CHILD_FIRST) as $filename => $file) {
            if (is_file($filename))
                unlink($filename);
            else
                rmdir($filename);
        }
        rmdir($dirname); // Now remove myfolder
    }
}

function glob_recursive($pattern, $flags = 0) {
    $files = glob($pattern, $flags);
    foreach (glob(dirname($pattern) . '/*', GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {
        $files = array_merge($files, glob_recursive($dir . '/' . basename($pattern), $flags));
    }
    return $files;
}

function cryptPassword($pass) {
    $pass = crypt($pass, Yii::$app->params['salt']);

    return $pass;
}
