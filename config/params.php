<?php

$params = [
    
    'appName' => 'Kompile Base',
    'shortName' => 'BaKom',
    'timeZone' => 'America/New_York',
    'lang' => 'es-CO',
    
    'adminEmail' => 'info@kompile.co',
    'hostEmail' => 'smtp.hostinger.co',
    'infoEmail' => 'mail@dev.kompile.co',
    'infoEmailPass' => 'Kompile2023*',
    'infoName' => 'Kompile Admin',
    
    'salt' => 'kompile_dev',
    'api' => 'Kompile',
    'passwordResetTokenExpire' => 3600,
    
    'iconActivo' => '<span class="glyphicon glyphicon-ok text-success"></span>',
    'iconInactivo' => '<span class="glyphicon glyphicon-remove text-danger"></span>',
    
    'maskMoneyOptions' => [
        'prefix' => 'USD $ ',
        'suffix' => '',
        'affixesStay' => true,
        'thousands' => ',',
        'decimal' => '.',
        'precision' => 0,
        'allowZero' => false,
        'allowNegative' => false,
    ],
        // 'analytics_id' => 'UA-67322800-4',
];


if (YII_ENV_PROD)
    $params = array_merge($params, [
        'db_host' => 'localhost',
        'db_user' => 'root',
        'db_pass' => '',
        'db_schema' => 'base',
        'google_key'=>'AIzaSyAf-DnCrLyEy-5wyxsO2F_2cf7zyYMA-kY'
    ]);
else
    $params = array_merge($params, [
        'db_host' => 'localhost',
        'db_user' => 'root',
        'db_pass' => '',
        'db_schema' => 'base',
        'google_key'=>'AIzaSyAf-DnCrLyEy-5wyxsO2F_2cf7zyYMA-kY'
        
    ]);

return $params;

