<?php

namespace app\controllers;

use app\models\ChangeForm;
use app\models\LoginForm;
use app\models\RememberForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'logout'],
                'rules' => [
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            $this->layout = 'main-login';
            //dd($exception);
            return $this->render('error', ['exception' => $exception]);
        }
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        return $this->render('dashboard', []);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            // dd(Yii::$app->user);
            return ($this->goBack());
        }

        $this->layout = 'main-login';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    //public function actionTest() {
    //  if (Yii::$app->Festivos->esFestivo(4, 6)) {
    //      echo'Festivo';
    //  }
    //$this->layout = 'main-login';
    //return $this->renderPartial('@app/mail/layouts/html');
    //}

    public function actionRemember()
    {

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new RememberForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Revisa tu email y sigue las instrucciones');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('danger', 'No pudimos enviar el mail, intenta de nuevo');
            }
        }

        $this->layout = 'main-login';
        return $this->render('remember', [
            'model' => $model,
        ]);
    }

    public function actionChange($token, $nueva = false)
    {
        //dd($token);
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new ChangeForm($token);

        $validation = $model->validateToken();
        if ($validation !== true) {
            Yii::$app->session->setFlash('danger', $validation);
            return $this->goHome();
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            $msg = $nueva ? 'Contraseña creada correctamente' : 'Contraseña actualizada correctamente';

            Yii::$app->session->setFlash('success', $msg);

            return $this->goHome();
        }

        $this->layout = 'main-login';
        return $this->render('change', [
            'model' => $model, 'new' => $nueva,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    /**
     * Displays about page.
     *
     * @return string
     */
}
