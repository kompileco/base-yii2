<?php

namespace app\controllers;


use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

class SelectController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    '*' => ['post'],
                ],
            ],
        ];
    }

    public function actionSelectRequestedCount()
    {

        return ['data' => Services::getRequestedCount()];
    }

    public function beforeAction($action)
    {

        Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }

}
