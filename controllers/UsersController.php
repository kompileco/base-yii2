<?php

namespace app\controllers;

use app\models\RememberForm;
use app\models\Users;
use app\models\UsersSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UsersController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'delete', 'create', 'update', 'remember-pass'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Users::validateRol(Users::ROL_ADMIN);
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$dataProvider->query->andWhere(['rol' => Users::ROL_USER]);

        $dataProvider->sort->defaultOrder = ['enabled' => SORT_DESC, 'id' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Users();

        if ($model->load(Yii::$app->request->post())) {

            $model->admin = Yii::$app->request->post('Users')['admin'];

            //dd($model->admin);
            $model->rol = ($model->admin) ? Users::ROL_ADMIN : Users::ROL_USER;

            if ($model->save()) {
                $modelPass = new RememberForm();
                $modelPass->email = $model->email;


                if ($modelPass->sendEmailNew()) {
                    Yii::$app->session->setFlash('success', 'Usuario creado correctamente');
                } else {
                    Yii::$app->session->setFlash('danger', 'No pudimos enviar el mail, intenta de nuevo');
                    $model->delete();
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }

                //Resetear token
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                // dd($model->getErrors());
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {

            $model->enabled = STATUS_ACTIVO;
            //$model->genero_id=Genero::GENERO_M;
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->admin = Yii::$app->request->post('Users')['admin'];

            //dd($model->admin);
            $model->rol = ($model->admin) ? Users::ROL_ADMIN : Users::ROL_USER;

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Usuario actualizado correctamente');
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                $model->password = "";
                return $this->render('update', [
                    'model' => $model,
                ]);
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $model->admin = $model->rol == Users::ROL_ADMIN;
            $model->password = "";
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->identity->id == $id || Users::validateRol(Users::ROL_ADMIN, $id)) {

            Yii::$app->session->setFlash('danger', 'No se puede eliminar un usuario Admin');
            return $this->redirect(['index']);
        }

        $this->findModel($id)->delete();

        Yii::$app->session->setFlash('success', 'Usuario eliminado correctamente');
        return $this->redirect(['index']);
    }


    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {

        if (($model = Users::findOne($id)) !== null) {

            if (Users::validateRol(Users::ROL_ADMIN)) {
                return $model;
            } else {
                throw new ForbiddenHttpException('No tienes permiso para acceder a este elemento');
            }
        } else {
            throw new NotFoundHttpException('Usuario no existe');
        }
    }
}
