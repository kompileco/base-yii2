CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(65) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `rol` tinyint(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#Password 123456
INSERT INTO `users` (`id`,`name`,`email`,`password`,`password_reset_token`,`auth_key`,`enabled`,`rol`,`created_at`,`updated_at`) VALUES (3,'Leonardo Sapuy','prueba@kompile.co','kobQfOV5gKwQk',NULL,'','1','1','2017-09-14 09:23:25','2017-09-17 02:31:19');



