<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user common\models\User */
//$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['cambiar', 'token' => $user->password_reset_token]);
$resetLink = Url::to(['site/change', 'token' => $user->password_reset_token], true);
?>

<table class="vb-outer" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#f0f0f0" style="background-color: #f0f0f0;" id="ko_textBlock_4">
    <tbody>
        <tr>
            <td class="vb-outer" align="center" valign="top" bgcolor="#f0f0f0" style="padding-left: 9px; padding-right: 9px; background-color: #f0f0f0;">

                                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="570"><tr><td align="center" valign="top"><![endif]-->
                <div class="oldwebkit" style="max-width: 570px;">
                    <table width="570" border="0" cellpadding="0" cellspacing="18" class="vb-container fullpad" bgcolor="#fff" style="border-collapse: separate; border-spacing: 18px; padding-left: 0; padding-right: 0; width: 100%; max-width: 570px; background-color: #fff;">
                        <tbody>
                            <tr>
                                <td align="left" class="long-text links-color" style="text-align: left; font-size: 15px;   color: #26292E;">
                                    <p style="Margin: 1em 0px; Margin-top: 0px; font-size: 17px;"><strong>Cambiar contraseña</strong></p>
                                   
                                    <p style="Margin: 1em 0px; Margin-bottom: 0px;">Sabemos que es dificil recordar tantas contraseñas, puedes crear una nueva haciendo click en el siguiente botón</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->
            </td>
        </tr>
    </tbody>
</table>
<table class="vb-outer" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#f0f0f0" style="background-color: #f0f0f0;" id="ko_buttonBlock_5">
    <tbody>
        <tr>
            <td class="vb-outer" align="center" valign="top" bgcolor="#f0f0f0" style="padding-left: 9px; padding-right: 9px; background-color: #f0f0f0;">

                                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="570"><tr><td align="center" valign="top"><![endif]-->
                <div class="oldwebkit" style="max-width: 570px;">
                    <table width="570" border="0" cellpadding="0" cellspacing="18" class="vb-container fullpad" bgcolor="#fff" style="border-collapse: separate; border-spacing: 18px; padding-left: 0; padding-right: 0; width: 100%; max-width: 570px; background-color: #fff;">
                        <tbody>
                            <tr>
                                <td valign="top" bgcolor="#fff" align="center" style="background-color: #fff;">

                                    <table cellpadding="0" border="0" align="center" cellspacing="0" class="mobile-full">
                                        <tbody>
                                            <tr>
                                                <td width="auto" valign="middle" bgcolor="#6355f2" align="center" height="50" style="font-size: 15px;   color: #fff; font-weight: normal; background-color: #6355f2; border-radius: 0px;">
                                                    <a style='color: #fff; text-decoration: none;padding: 15px; 'href='<?= ($resetLink) ?>'>Change password</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->
            </td>
        </tr>
    </tbody>
</table>
<table class="vb-outer" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#f0f0f0" style="background-color: #f0f0f0;" id="ko_textBlock_6">
    <tbody>
        <tr>
            <td class="vb-outer" align="center" valign="top" bgcolor="#f0f0f0" style="padding-left: 9px; padding-right: 9px; background-color: #f0f0f0;">

                                <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="570"><tr><td align="center" valign="top"><![endif]-->
                <div class="oldwebkit" style="max-width: 570px;">
                    <table width="570" border="0" cellpadding="0" cellspacing="18" class="vb-container fullpad" bgcolor="#fff" style="border-collapse: separate; border-spacing: 18px; padding-left: 0; padding-right: 0; width: 100%; max-width: 570px; background-color: #fff;">
                        <tbody>
                            <tr>
                                <td align="left" class="long-text links-color" style="text-align: left; font-size: 15px;   color: #26292E;">
                                 <p style="Margin: 1em 0px; Margin-bottom: 0px; Margin-top: 0px;">Si no puedes hacer click en el botón, sólo copia y pega este link en tu navegador favorito</p>
                                    <p style="Margin: 1em 0px; Margin-bottom: 0px; Margin-top: 0px;"><a style='color:#933ff1;'href='<?= ($resetLink) ?>'><?= ($resetLink) ?></p>
                                  </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->
            </td>
        </tr>
    </tbody>
</table>


