<?php

use yii\helpers\Url;
use yii\helpers\Html;
//dd(get_defined_vars());
/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage()   ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <meta name="viewport" content="initial-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
    <title><?= Yii::$app->name ?></title>
    <style type="text/css">
        body {
            Margin: 0;
            padding: 0;
        }

        img {
            border: 0px;
            display: block;
        }

        .socialLinks {
            font-size: 6px;
        }

        .socialLinks a {
            display: inline-block;
        }

        .socialIcon {
            display: inline-block;
            vertical-align: top;
            padding-bottom: 0px;
            border-radius: 100%;
        }

        .oldwebkit {
            max-width: 570px;
        }

        td.vb-outer {
            padding-left: 9px;
            padding-right: 9px;
        }

        table.vb-container,
        table.vb-row,
        table.vb-content {
            border-collapse: separate;
        }

        table.vb-row {
            border-spacing: 9px;
        }

        table.vb-row.halfpad {
            border-spacing: 0;
            padding-left: 9px;
            padding-right: 9px;
        }

        table.vb-row.fullwidth {
            border-spacing: 0;
            padding: 0;
        }

        table.vb-container {
            padding-left: 18px;
            padding-right: 18px;
        }

        table.vb-container.fullpad {
            border-spacing: 18px;
            padding-left: 0;
            padding-right: 0;
        }

        table.vb-container.halfpad {
            border-spacing: 9px;
            padding-left: 9px;
            padding-right: 9px;
        }

        table.vb-container.fullwidth {
            padding-left: 0;
            padding-right: 0;
        }
    </style>
    <style type="text/css">
        /* yahoo, hotmail */

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }

        .yshortcuts a {
            border-bottom: none !important;
        }

        .vb-outer {
            min-width: 0 !important;
        }

        .RMsgBdy,
        .ExternalClass {
            width: 100%;
            background-color: #3f3f3f;
            background-color: #26292E
        }

        /* outlook */

        table {
            mso-table-rspace: 0pt;
            mso-table-lspace: 0pt;
        }

        #outlook a {
            padding: 0;
        }

        img {
            outline: none;
            text-decoration: none;
            border: none;
            -ms-interpolation-mode: bicubic;
        }

        a img {
            border: none;
        }

        @media screen and (max-device-width: 600px),
        screen and (max-width: 600px) {

            table.vb-container,
            table.vb-row {
                width: 95% !important;
            }

            .mobile-hide {
                display: none !important;
            }

            .mobile-textcenter {
                text-align: center !important;
            }

            .mobile-full {
                float: none !important;
                width: 100% !important;
                max-width: none !important;
                padding-right: 0 !important;
                padding-left: 0 !important;
            }

            img.mobile-full {
                width: 100% !important;
                max-width: none !important;
                height: auto !important;
            }
        }
    </style>
    <style type="text/css">
        #ko_textBlock_4 .links-color a,
        #ko_textBlock_4 .links-color a:link,
        #ko_textBlock_4 .links-color a:visited,
        #ko_textBlock_4 .links-color a:hover {
            color: #3f3f3f;
            color: #3f3f3f;
            text-decoration: none
        }

        #ko_textBlock_6 .links-color a,
        #ko_textBlock_6 .links-color a:link,
        #ko_textBlock_6 .links-color a:visited,
        #ko_textBlock_6 .links-color a:hover {
            color: #3f3f3f;
            color: #3f3f3f;
            text-decoration: none
        }

        #ko_textBlock_4 .long-text p {
            Margin: 1em 0px
        }

        #ko_textBlock_6 .long-text p {
            Margin: 1em 0px
        }

        #ko_textBlock_4 .long-text p:last-child {
            Margin-bottom: 0px
        }

        #ko_textBlock_6 .long-text p:last-child {
            Margin-bottom: 0px
        }

        #ko_textBlock_4 .long-text p:first-child {
            Margin-top: 0px
        }

        #ko_textBlock_6 .long-text p:first-child {
            Margin-top: 0px
        }

        #ko_footerBlock_2 .links-color a,
        #ko_footerBlock_2 .links-color a:link,
        #ko_footerBlock_2 .links-color a:visited,
        #ko_footerBlock_2 .links-color a:hover {
            color: #cccccc;
            color: #cccccc;
            text-decoration: none
        }

        #ko_footerBlock_2 .long-text p {
            Margin: 1em 0px
        }

        #ko_footerBlock_2 .long-text p:last-child {
            Margin-bottom: 0px
        }

        #ko_footerBlock_2 .long-text p:first-child {
            Margin-top: 0px
        }
    </style>
</head>

<body bgcolor="#26292E" text="#919191" alink="#cccccc" vlink="#cccccc" style="Margin: 0; padding: 0; background-color: #26292E; color: #919191;">

    <center>


        <table class="vb-outer" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#f0f0f0" style="background-color: #f0f0f0;" id="ko_titleBlock_3">
            <tbody>
                <tr>
                    <td class="vb-outer" align="center" valign="top" bgcolor="#f0f0f0" style="padding-left: 9px; padding-right: 9px; background-color: #f0f0f0;">

                        <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="570"><tr><td align="center" valign="top"><![endif]-->
                        <div class="oldwebkit" style="max-width: 570px;">
                            <table width="570" border="0" cellpadding="0" cellspacing="9" class="vb-container halfpad" bgcolor="#fff" style="border-collapse: separate; border-spacing: 9px; padding-left: 9px; padding-right: 9px; width: 100%; max-width: 570px; background-color: #fff;" align="center">
                                <tbody>
                                    <tr>
                                        <td bgcolor="#fff" align="center" style="background-color: #fff; font-size: 24px;   color: #6355f2; text-align: center;">
                                            <a style='text-decoration:none;color:#26292E;' href='<?= Url::home(true) ?>'>
                                                <?= Html::img(Url::home(true) . '/img/logo1.png', ['height' => '40', 'style' => 'margin-left:auto;margin-right: auto;']) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->
                    </td>
                </tr>
            </tbody>
        </table>
        <?= $content //Yii::$app->controller->renderPartial('@app/mail/recordarPassword')  
        ?>
        <!-- footerBlock -->
        <table width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#26292E" style="background-color: #26292E;" id="ko_footerBlock_2">
            <tbody>
                <tr>
                    <td align="center" valign="top" bgcolor="#26292E" style="background-color: #26292E;">

                        <!--[if (gte mso 9)|(lte ie 8)]><table align="center" border="0" cellspacing="0" cellpadding="0" width="570"><tr><td align="center" valign="top"><![endif]-->
                        <div class="oldwebkit" style="max-width: 570px;">
                            <table width="570" style="border-collapse: separate; border-spacing: 9px; padding-left: 9px; padding-right: 9px; width: 100%; max-width: 570px;" border="0" cellpadding="0" cellspacing="9" class="vb-container halfpad" align="center">
                                <tbody>
                                    <tr>
                                        <td class="long-text links-color" style="text-align: center; font-size: 13px; color: #fff; font-weight: normal; text-align: center;  ">
                                            <p style="Margin: 1em 0px; Margin-bottom: 0px; Margin-top: 0px;"><?= Yii::$app->params['appName'] ?></p>

                                            <!--<p style="Margin: 1em 0px; Margin-bottom: 0px; Margin-top: 0px;"><span>Miami - USA</span></p>-->

                                            <p style="Margin: 1em 0px; Margin-bottom: 0px; Margin-top: 0px;"><a href="mailto:<?= Yii::$app->params['adminEmail'] ?>" style="color: #cccccc; color: #6355f2; text-decoration: none;"><?= Yii::$app->params['adminEmail'] ?></a></p>


                                        </td>
                                    </tr>


                                </tbody>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(lte ie 8)]></td></tr></table><![endif]-->
                    </td>
                </tr>
            </tbody>
        </table>
        <!-- /footerBlock -->
    </center>

</body>

</html>

<?php
$this->endPage() ?>