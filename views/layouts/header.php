<?php

use yii\helpers\Html;
use yii\helpers\Url;
use richardfan\widget\JSRegister;
/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini"><span class="md">' . Html::img('@web/img/icon.png', ['height' => '35']) . '</span></span>
	<span class="logo-lg">' . Html::img('@web/img/logo2.png', ['height' => '35']) . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">




                <!-- User Account: style can be found in dropdown.less -->
                <?php if (!Yii::$app->user->isGuest) : ?>

                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span id='label-count' class="label label-warning"><?= '2' ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">Last 5 requested services</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    <li>

                                        <a target=_blank href="#">
                                            <i class="fa fa-asterisk text-aqua"></i> <?= 'abc - 123' ?>
                                        </a>

                                        <a href="#">
                                            <i class="fa fa-ban"></i> There's no services in "requested" status
                                        </a>


                                    </li>
                                </ul>
                            </li>
                            <li class="footer"><a href="<?= Url::to(['services/index']) ?>">Ver todos</a></li>
                        </ul>
                    </li>

                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user"></i>
                            <span class="hidden-xs"><?= Yii::$app->user->identity->name ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <?= Html::img('@web/img/icon.png', ['height' => '35', 'class' => 'img-circle']) ?>


                                <p>
                                    <?= Yii::$app->user->identity->name ?>
                                    <small><?= Yii::$app->user->identity->email ?></small>
                                </p>
                            </li>
                            <!-- Menu Body 
                            <li class="user-body">
                                <div class="col-xs-4 text-center">
                                    <a href="#">Followers</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Sales</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Friends</a>
                                </div>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <!--<a href="#" class="btn btn-default btn-flat">Profile</a>-->
                                </div>
                                <div class="pull-right">
                                    <?=
                                    Html::a(
                                        'Cerrar sesión',
                                        [Url::to('site/logout')],
                                        ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                    )
                                    ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                <?php endif; ?>

                <!-- User Account: style can be found in dropdown.less -->

            </ul>
        </div>
    </nav>
</header>
<?php JSRegister::begin(['position' => static::POS_END]); ?>
<script>
    window.setInterval(function() {

        $.post("<?= Url::to(['select/select-requested-count']) ?>", function(data) {
            if (data.hasOwnProperty('data')) {
                $("#label-count").text(data.data);

            }
        });


    }, 60 * 1000);
</script>
<?php JSRegister::end(); ?>