<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = 'View <?= Inflector::singularize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))?>';
$this->params['breadcrumbs'][] = ['label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = $model-><?= $generator->getNameAttribute() ?>;
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-view">

    <div class="box box-primary">
        <div class="box-body">

            <?php
            echo "<?php\n";
            ?>
            $button1 = Html::a('<i class="glyphicon glyphicon-trash"></i>', Url::to(['delete', <?= $urlParams ?>]), [
            'title' => 'Delete',
            'class' => 'pull-right detail-button',
            'data' => [
            'confirm' => <?= $generator->generateString('¿Deseas eliminar este elemento?') ?>,
            'method' => 'post',
            ]
            ]);
            $button2 = Html::a('<i class="glyphicon glyphicon-pencil"></i>', Url::to(['update', <?= $urlParams ?>]), [
            'title' => 'Update',
            'class' => 'pull-right detail-button',]);

            <?php
            echo '?>';
            ?>

            <?= "<?= " ?>DetailView::widget([
            'model' => $model,
            'hover' => true,
            'hideAlerts' => true,
            'enableEditMode' => false,  'notSetIfEmpty'=>true,
            'mode' => DetailView::MODE_VIEW,
            'hAlign' => 'right',
            'panel' => [
            'heading' =>  $model-><?= $generator->getNameAttribute() ?>,
            'type' => DetailView::TYPE_DEFAULT,
            'buttons1' => "$button1 $button2",
            ],
            'attributes' => [
            <?php
            if (($tableSchema = $generator->getTableSchema()) === false) {
                foreach ($generator->getColumnNames() as $name) {
                    echo "            '" . $name . "',\n";
                }
            } else {
                foreach ($generator->getTableSchema()->columns as $column) {
                    $format = $generator->generateColumnFormat($column);
                    echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                }
            }
            ?>
            ],
            ]) ?>

        </div></div></div>
