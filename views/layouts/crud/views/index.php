<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\helpers\Url;
use <?= $generator->indexWidgetType === 'grid' ? "kartik\\grid\\GridView" : "yii\\widgets\\ListView" ?>;
<?= $generator->enablePjax ? 'use yii\widgets\Pjax;' : '' ?>

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>;
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index">


    <div class="box box-primary">
        <div class="box-body">

            <?php if (!empty($generator->searchModelClass)): ?>
                <?= "    <?php " . ($generator->indexWidgetType === 'grid' ? "// " : "") ?>echo $this->render('_search', ['model' => $searchModel]); ?>
            <?php endif; ?>


            <?= $generator->enablePjax ? '<?php Pjax::begin(); ?>' : '' ?>
            <?php if ($generator->indexWidgetType === 'grid'): ?>
                <?= "<?= " ?>GridView::widget([
                'dataProvider' => $dataProvider,
                <?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel,\n        'columns' => [\n" : "'columns' => [\n"; ?>
                ['class' => 'kartik\grid\SerialColumn'],

                <?php
                $count = 0;
                if (($tableSchema = $generator->getTableSchema()) === false) {
                    foreach ($generator->getColumnNames() as $name) {
                        if (++$count < 6) {
                            echo "            '" . $name . "',\n";
                        } else {
                            echo "            // '" . $name . "',\n";
                        }
                    }
                } else {
                    foreach ($tableSchema->columns as $column) {
                        $format = $generator->generateColumnFormat($column);
                        if (++$count < 6) {
                            echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                        } else {
                            echo "            // '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                        }
                    }
                }
                ?>

                /* ['class' => 'kartik\grid\ActionColumn',
                'template' => '{view}{update}{delete}',
                'buttons' => [
                'view' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                'title' => Yii::t('app', 'lead-view'),
                ]);
                },

                'update' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                'title' => Yii::t('app', 'lead-update'),
                ]);
                },


                ],],*/
                ],
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                'toolbar' =>  [
                ['content' => 
                Html::a('<i class="glyphicon glyphicon-plus"></i>', Url::to('create'),[ 'title' => 'Nuevo <?= strtolower(StringHelper::basename($generator->modelClass)) ?>', 'class' => 'btn btn-success',]) . 
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', Url::current(), ['class' => 'btn btn-default', 'title' => 'Refrescar'])
                ],
                '{export}',
                '{toggleData}',
                ],
                'export' => [
                'fontAwesome' => true
                ],	
                'striped' => true,
                'responsive' => true,
                'hover' => true,
                'showPageSummary' => false,
                'panel' => [
                'type' => GridView::TYPE_DEFAULT,

                ],
                'persistResize' => true,
                'toggleDataOptions' => ['minCount' => 10],
                'itemLabelSingle' => <?= strtolower($generator->generateString(Inflector::singularize(StringHelper::basename($generator->modelClass)))) ?>,
                'itemLabelPlural' => <?= strtolower($generator->generateString(Inflector::pluralize(StringHelper::basename($generator->modelClass)))) ?>
                ]); ?>
            <?php else: ?>
                <?= "<?= " ?>ListView::widget([
                'dataProvider' => $dataProvider,
                'itemOptions' => ['class' => 'item'],
                'itemView' => function ($model, $key, $index, $widget) {
                return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
                },
                ]) ?>
            <?php endif; ?>
            <?= $generator->enablePjax ? '<?php Pjax::end(); ?>' : '' ?>
        </div>
    </div>
</div>