<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
?>
<div class='panel panel-default'>
    <div class='panel-heading'>
        <h3 class="panel-title"> <?= "<?= " ?>$model->isNewRecord ? 'Información 
            <?= Inflector::camel2words(Inflector::singularize(StringHelper::basename($generator->modelClass))) ?>' :
            $model-><?= $generator->getNameAttribute() ?> <?= "?> " ?></h3>

    </div>
    <div class='panel-body'>

        <div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">

            <?= "<?php " ?>$form = ActiveForm::begin(['id' =>
            '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form',
            'type'=>ActiveForm::TYPE_HORIZONTAL]); ?>

            <?php
            foreach ($generator->getColumnNames() as $attribute) {
                if (in_array($attribute, $safeAttributes)) {
                    echo "    <?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
            }
            }
            ?>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <hr>
                    <?= "<?= " ?>Html::submitButton($model->isNewRecord ? <?= $generator->generateString('Crear') ?> :
                    <?= $generator->generateString('Actualizar') ?>, ['class' => $model->isNewRecord ? 'btn btn-success' :
                    'btn btn-flat']) ?>
                    <?= "<?= " ?>Html::resetButton('Reiniciar', ['class' => 'btn btn-default']) ?>
                </div>
            </div>

            <?= "<?php " ?>ActiveForm::end(); ?>

        </div>
    </div>
</div>