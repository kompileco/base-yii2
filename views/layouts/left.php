<?php

use yii\helpers\Url;
use app\models\Users;
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel 
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form  -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Buscar..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?php
        //  dd(Url::current());

        if (Users::validateRol(Users::ROL_ADMIN)) {
            $menu = [
                ['label' => 'Dashboard', 'options' => ['class' => 'header']],
                ['label' => 'Home', 'icon' => 'dashboard', 'url' => [Url::to('/')], 'active' => (Url::current() == Url::home() . 'index') ? true : false],
                ['label' => 'Title', 'options' => ['class' => 'header']],
                [
                    'label' => 'Menu1',
                    'icon' => 'asterisk',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Create', 'icon' => 'plus', 'url' => [Url::to('/services/create')],],
                        ['label' => 'List', 'icon' => 'list', 'url' => [Url::to('/services/index')], 'active' => strpos(Url::current(), 'services') && !strpos(Url::current(), 'create') ? true : false],
                    ],
                ],
                //  ['label' => 'Users management', 'options' => ['class' => 'header']],
                [
                    'label' => 'Usuarios',
                    'icon' => 'users',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Crear', 'icon' => 'plus', 'url' => [Url::to('/users/create')],],
                        ['label' => 'Listado', 'icon' => 'list', 'url' => [Url::to('/users/index')], 'active' => strpos(Url::current(), 'users') && !strpos(Url::current(), 'create') ? true : false],
                    ],
                ],
           
                ['label' => 'System parameters', 'options' => ['class' => 'header']],
                [
                    'label' => 'Cities',
                    'icon' => 'globe',
                    'url' => [Url::to('/cities/index')],
                    'active' => strpos(Url::current(), 'cities')
                ],
            ];
        }



        echo (!Yii::$app->user->isGuest) ? dmstr\widgets\Menu::widget([
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => $menu
                ]) : ''
        ?>

    </section>

</aside>
