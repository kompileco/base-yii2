<?php

use yii\helpers\Html;
use yii\helpers\Url;
use richardfan\widget\JSRegister;
use cybercog\yii\googleanalytics\widgets\GATracking;

/* @var $this \yii\web\View */
/* @var $content string */
app\assets\AppAsset::register($this);

$directoryAsset = Url::to('@web');
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => Url::to(['/img/favicon.png'])]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode(Yii::$app->params['shortName'].' | ' . $this->title) ?></title> 
        <?php $this->head() ?>

    </head>
    <body class="hold-transition fixed skin-yellow sidebar-mini ">
        <?php $this->beginBody() ?>
        <div class="wrapper">

            <?=
            $this->render(
                    'header.php', ['directoryAsset' => $directoryAsset]
            )
            ?>

            <?=
            $this->render(
                    'left.php', ['directoryAsset' => $directoryAsset]
            )
            ?>

            <?=
            $this->render(
                    'content.php', ['content' => $content, 'directoryAsset' => $directoryAsset]
            )
            ?>

        </div>

        <?php JSRegister::begin(['position' => static::POS_BEGIN]); ?>
        <script>

            (function () {
                if (Boolean(sessionStorage.getItem('sidebar-toggle-collapsed'))) {
                    var body = document.getElementsByTagName('body')[0];
                    body.className = body.className + ' sidebar-collapse';
                }
            })();
        </script>

        <?php JSRegister::end(); ?>


        <?php JSRegister::begin(['position' => static::POS_END]); ?>
        <script>
          /* (function () {
                var options = {
                    whatsapp: "+573143732575", // WhatsApp number
                    call_to_action: "¿Necesitas soporte técnico?", // Call to action
                    position: "right", // Position may be 'right' or 'left'
                };
                var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
                var s = document.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = url + '/widget-send-button/js/init.js';
                s.onload = function () {
                    WhWidgetSendButton.init(host, proto, options);
                };
                var x = document.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
            })();*/

        </script>

        <?php JSRegister::end(); ?>

        <?php JSRegister::begin(['position' => static::POS_END]); ?>
        <script>

            // Click handler can be added latter, after jQuery is loaded...
            $('.sidebar-toggle').click(function (event) {
                event.preventDefault();
                if (Boolean(sessionStorage.getItem('sidebar-toggle-collapsed'))) {
                    sessionStorage.setItem('sidebar-toggle-collapsed', '');
                } else {
                    sessionStorage.setItem('sidebar-toggle-collapsed', '1');
                }
            });
        </script>

        <?php JSRegister::end(); ?>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>

