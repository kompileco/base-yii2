<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Ver Usuario';
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="users-view">

    <div class="box box-primary">
        <div class="box-body">

            <?php
            $button1 = Html::a('<i class="glyphicon glyphicon-trash"></i>', Url::to(['delete', 'id' => $model->id]), [
                'title' => 'Delete',
                'class' => 'pull-right detail-button',
                'data' => [
                    'confirm' => '¿Deseas eliminar este elemento?',
                    'method' => 'post',
                ]
            ]);
            $button2 = Html::a('<i class="glyphicon glyphicon-pencil"></i>', Url::to(['update', 'id' => $model->id]), [
                'title' => 'Update',
                'class' => 'pull-right detail-button',
            ]);
         
            ?>
            <?=
            DetailView::widget([
                'model' => $model,
                'hover' => true,
                'hideAlerts' => true,
                'enableEditMode' => false, 'notSetIfEmpty' => true,
                'mode' => DetailView::MODE_VIEW,
                'hAlign' => 'right',
                'panel' => [
                    'heading' => $model->name,
                    'type' => DetailView::TYPE_ACTIVE,
                   
                ],
                'buttons1' => "$button1 $button2",
                'attributes' => [
                    'id',
                    'name',

                    'email:email',

                    [
                        'attribute' => 'rol',
                        'format' => 'raw',
                        'value' => ($model->rol == app\models\Users::ROL_ADMIN?'Admin':'Usuario'),
                    ],
                    [
                        'attribute' => 'enabled',
                        'format' => 'raw',
                        'value' => showIconStatus($model->enabled),
                    ],

                    'created_at',
                    // 'rol',

                    'updated_at',
                ],
            ])
            ?>

        </div>
    </div>
</div>