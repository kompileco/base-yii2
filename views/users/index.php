<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarios';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="users-index">


    <div class="box box-primary">
        <div class="box-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>


            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'kartik\grid\SerialColumn'],
                    'id',

                    [
                        'attribute' => 'name',
                        'label' => 'Nombre',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $widget) {

                            return Html::a($model->name, Url::to(['view', 'id' => $model->id]), ['title' => 'View details']);
                        },
                    ],

                    // 'address',
                    // 'age',
                    // 'id_city',
                    // 'created_at',
                    // 'updated_at',
                    'email:email',
                    // 'rol',
                    [
                        'attribute' => 'rol',
                        'label' => 'Rol',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $column) {

                            return $model->rol == app\models\Users::ROL_ADMIN?'Admin':'Usuario';
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => [app\models\Users::ROL_ADMIN => 'Admin', app\models\Users::ROL_USER => 'User'],
                        'filterWidgetOptions' => [
                            'hideSearch' => true,
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Todos'],
                    ],
                    [
                        'attribute' => 'enabled',
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $column) {

                            return showIconStatus($model->enabled);
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => [STATUS_ACTIVO => 'Enabled', STATUS_INACTIVO => 'Disabled'],
                        'filterWidgetOptions' => [
                            'hideSearch' => true,
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'Todos'],
                    ],
                    // 'password',
                    // 'password_reset_token',
                    // 'auth_key',

                    /* ['class' => 'kartik\grid\ActionColumn',
                  'template' => '{view}{update}{delete}',
                  'buttons' => [
                  'view' => function ($url, $model) {
                  return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                  'title' => Yii::t('app', 'lead-view'),
                  ]);
                  },

                  'update' => function ($url, $model) {
                  return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                  'title' => Yii::t('app', 'lead-update'),
                  ]);
                  },


                  ],], */
                ],
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                'toolbar' => [
                    [
                        'content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', Url::to('create'), ['title' => 'Nuevo usuario', 'class' => 'btn btn-success',]) .
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', Url::current(), ['class' => 'btn btn-default', 'title' => 'Refrescar'])
                    ],
                    '{export}',
                    '{toggleData}',
                ],
                'export' => [
                    'fontAwesome' => true
                ],
                'striped' => true,
                'responsive' => true,
                'hover' => true,
                'showPageSummary' => false,
                'panel' => [
                    'type' => GridView::TYPE_DEFAULT,
                ],
                'persistResize' => true,
                'toggleDataOptions' => ['minCount' => 10],
                'itemLabelSingle' => 'usuario',
                'itemLabelPlural' => 'usuarios'
            ]);
            ?>
        </div>
    </div>
</div>