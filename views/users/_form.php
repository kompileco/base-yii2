<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>
<div class='panel panel-default'>
    <div class='panel-heading'>
        <h3 class="panel-title"> <?= $model->isNewRecord ? 'Información de Usuario' : $model->name ?> </h3>

    </div>
    <div class='panel-body'>

        <div class="users-form">

            <?php $form = ActiveForm::begin(['id' => 'users-form', 'type' => ActiveForm::TYPE_HORIZONTAL]); ?>


            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>



            <?=
            $form->field($model, 'admin')->widget(SwitchInput::classname(), ['pluginOptions' => [
                'onText' => '<i class="glyphicon glyphicon-ok"></i>',
                'offText' => '<i class="glyphicon glyphicon-remove"></i>',
                'offColor' => 'danger',
                'onColor' => 'success',
                'handleWidth' => 20,
            ]]);
            ?>


            <?=
            $form->field($model, 'enabled')->widget(SwitchInput::classname(), ['pluginOptions' => [
                'onText' => '<i class="glyphicon glyphicon-ok"></i>',
                'offText' => '<i class="glyphicon glyphicon-remove"></i>',
                'offColor' => 'danger',
                'onColor' => 'success',
                'handleWidth' => 20,
            ]]);
            ?>



            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <hr>
                    <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-flat']) ?>
                    <?= Html::resetButton('Reiniciar', ['class' => 'btn btn-default']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>