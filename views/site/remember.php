<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Remember password';

?>

<div class="login-box">
    <div class="login-logo">
    <span class='md'><?= Html::img('@web/img/logo2.png', ['height' => '60']) ?></span>    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => true, 'type' => ActiveForm::TYPE_HORIZONTAL]); ?>
        <?= $form->field($model, 'email')->textInput() ?>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <hr>
                <?= Html::submitButton('Cambiar', ['class' => 'btn btn-flat', 'name' => 'login-button']) ?>
                <?= Html::resetButton('Reiniciar', ['class' => 'btn btn-default']) ?>
                <hr>
                <a href='<?= Url::to('login') ?>'>Iniciar sesión</a> 
            </div>
        </div>

     

        <?php ActiveForm::end(); ?>

    </div>

</div>
