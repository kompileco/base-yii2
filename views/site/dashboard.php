<?php

use app\models\Charts;
/* @var $this yii\web\View */
use app\models\Services;
use app\models\Users;
use dosamigos\chartjs\ChartJs;
use cjtterabytesoft\widget\jvectormap\JvectorMap;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::$app->name;
$class = '6';
?>

<div class="row">
    <?php
    if (Users::validateRol(Users::ROL_ADMIN)) {
        $class = '3';
    ?>


        <div class="col-md-<?= $class ?> col-sm-12 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?= Users::getIndexCount() ?></h3>

                    <p>Users</p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="<?= Url::to('users/index') ?>" class="small-box-footer">Ver todos <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    <?php
    }
    ?>

    <!-- ./col -->
    <div class="col-md-<?= $class ?> col-sm-12 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3><?= '1' ?></h3>

                <p>Services</p>
            </div>
            <div class="icon">
                <i class="fa fa-asterisk"></i>
            </div>
            <a href="<?= Url::to('services/index') ?>" class="small-box-footer">Ver todos <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-md-<?= $class ?> col-sm-12 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>0</h3>

                <p>Item3</p>
            </div>
            <div class="icon">
                <i class="fa fa-image"></i>
            </div>
            <a href="#" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <!-- ./col -->
    <div class="col-md-<?= $class ?> col-sm-12 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-olive">
            <div class="inner">
                <h3>0</h3>

                <p>Item4</p>
            </div>
            <div class="icon">
                <i class="fa  fa-battery-half"></i>
            </div>
            <a href="#" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>


    <!-- ./col -->
</div>


<div class="row">
    <div class="col-md-12">
        <!-- MAP & BOX PANE -->
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Visitors Report</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <div class="row no-padding">
                    <div class="col-md-8">
                    <?= JvectorMap::widget([
                        /** [div container] **/
                        'id' => 'vmap',
                        'style' => [
                            
                            'height' => '405px',
                            'position' => 'relative',
                            'overflow' => 'hidden',
                        ],
                        /** [map config] **/
                        'map' => 'co_merc',
                        'maptype' => true,
                        'backgroundColor' => '#fefefe',
                        'focusOn' => [
                            'CO-AMA',
                            'CO-DC',
                            'CO-LAG',
                        ],
                        'markers' => [
                          
                            [
                                'latLng' => [7.89391, -72.50782],
                                'name' => 'Cucuta : 300'
                            ],
                            [
                                'latLng' => [7.12539, -73.1198],
                                'name' => 'B/manga : 200'
                            ],
                        ],
                        'markersSelectable' => false,
                        'markersSelectableOne' => true,
                        'markerStyle' => [
                            'initial' => [
                                'r' => 7,
                                'fill' => '#fff',
                                'fill-opacity' => 1,
                                'stroke' => '#000',
                                'stroke-width' => 2,
                                'stroke-opacity' => 0.4,
                            ],
                        ],
                        'regionLabelStyle' => [
                            'initial' => [
                                'font-size' => '12',
                                'font-weight' => 'bold',
                                'cursor' => 'default',
                                'fill' => 'black',
                            ],
                            'hover' => [
                                'cursor' => 'pointer',
                            ],
                        ],
                        'selectedRegions'=> ['CO-NSA','CO-DC','CO-SAN'],
                        'regionStyle' => [
                            'initial' => [
                                'fill' => '#fed67d',
                            ],
                            'selected' => [
                                'fill' => '#6355f2'
                            ]
                        ],
                       /* 'series' => [
                            'regions' => [[
                                'values' => [
                                    'CO-AMA' => 0,
                                    'CO-LAG' => 150,
                                    'CO-NSA' => 300,
                                ],
                                'scale' => ['#03a9f3', '#02a7f1'],
                                'normalizeFunction' => 'polynomial',
                            ]],
                        ],*/
                        'zoomAnimate' => true,
                        'zoomMax' => 8,
                        'zoomMin' => 1,
                        'zoomOnScroll' => false,
                    ]); ?>
                    </div>
                    <div class="col-md-4">
                        <!-- Info Boxes Style 2 -->
                        <div class="info-box bg-yellow">
                            <span class="info-box-icon"><i class="fa fa-bank"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Inventory</span>
                                <span class="info-box-number">5,200</span>

                                <div class="progress">
                                    <div class="progress-bar" style="width: 50%"></div>
                                </div>
                                <span class="progress-description">
                                    50% Increase in 30 Days
                                </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        <div class="info-box bg-green">
                            <span class="info-box-icon"><i class="fa fa-heart"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Mentions</span>
                                <span class="info-box-number">92,050</span>

                                <div class="progress">
                                    <div class="progress-bar" style="width: 20%"></div>
                                </div>
                                <span class="progress-description">
                                    20% Increase in 30 Days
                                </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        <div class="info-box bg-maroon">
                            <span class="info-box-icon"><i class="fa fa-automobile"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Downloads</span>
                                <span class="info-box-number">114,381</span>

                                <div class="progress">
                                    <div class="progress-bar" style="width: 70%"></div>
                                </div>
                                <span class="progress-description">
                                    70% Increase in 30 Days
                                </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        <div class="info-box bg-aqua mb-0">
                            <span class="info-box-icon"><i class="fa fa-fire"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Direct Messages</span>
                                <span class="info-box-number">163,921</span>

                                <div class="progress">
                                    <div class="progress-bar" style="width: 40%"></div>
                                </div>
                                <span class="progress-description">
                                    40% Increase in 30 Days
                                </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->

                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>



</div>


<div class="row">

    <div class="col-md-6">
        <!-- AREA CHART -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Area Chart</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <?= ChartJs::widget([
                    'type' => 'doughnut',
                    'options' => [
                        'height' => 220,
                        'responsive' => true,
                        'label' => [
                            'display' => false,
                        ],
                    ],
                    'data' => [
                        'labels' => ["January", "February", "March", "April", "May", "June", "July"],
                        'datasets' => [

                            [
                                'label' => "My Second dataset",
                                'backgroundColor' => "rgba(236, 48, 164,0.7)",
                                'borderColor' => "#fed67d",
                                'pointBackgroundColor' => "rgba(255,99,132,1)",
                                'pointBorderColor' => "#fff",
                                'pointHoverBackgroundColor' => "#fff",
                                'pointHoverBorderColor' => "rgba(255,99,132,1)",
                                'data' => [28, 48, 40, 19, 96, 27, 100]
                            ]
                        ]
                    ]
                ]);
                ?>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <div class="col-md-6">
        <!-- DONUT CHART -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Donut Chart</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">

                <?= ChartJs::widget([
                    'type' => 'line',
                    'options' => [
                        'height' => 220,
                        'responsive' => true,
                    ],
                    'data' => [
                        'labels' => ["January", "February", "March", "April", "May", "June", "July"],
                        'datasets' => [
                            [
                                'label' => "My First dataset",
                                'backgroundColor' => "rgba(179,181,198,0.2)",
                                'borderColor' => "rgba(179,181,198,1)",
                                'pointBackgroundColor' => "rgba(179,181,198,1)",
                                'pointBorderColor' => "#fff",
                                'pointHoverBackgroundColor' => "#fff",
                                'pointHoverBorderColor' => "rgba(179,181,198,1)",
                                'data' => [65, 59, 90, 81, 56, 55, 40]
                            ],
                            [
                                'label' => "My Second dataset",
                                'backgroundColor' => "rgba(14,229,252,0.7)",
                                'borderColor' => "rgba(99,85,242,1)",
                                'pointBackgroundColor' => "rgba(255,99,132,1)",
                                'pointBorderColor' => "#fff",
                                'pointHoverBackgroundColor' => "#fff",
                                'pointHoverBorderColor' => "rgba(255,99,132,1)",
                                'data' => [28, 48, 40, 19, 96, 27, 100]
                            ]
                        ]
                    ]
                ]);
                ?>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>

</div>

<div class="row">

    <div class='col-xs-12 col-lg-6 '>
        <div class="box box-primary">
            <div class="box-header">
                Services requested in last week
            </div>
            <div class="box-body">

                <?=

                ChartJs::widget([
                    'type' => 'line',
                    'options' => [
                        'height' => 200,
                        'responsive' => true,
                        'label' => [
                            'display' => false,
                        ],
                    ],
                    'data' => [
                        'labels' => Charts::getLastWeek(),
                        'datasets' => [
                            [
                                'label' => 'Services',
                                'backgroundColor' => "rgba(99,85,242,0.7)",
                                'borderColor' => "#933FF1",
                                'pointBackgroundColor' => "#6355f2",
                                'pointBorderColor' => "#933FF1",
                                'pointHoverBackgroundColor' => "#933FF1",
                                'pointHoverBorderColor' => "#933FF1",
                                'data' => Charts::getLastWeekServices(),
                            ],
                        ],
                    ],
                ]);
                ?>


            </div>
        </div>
    </div>

    <div class='col-xs-12 col-lg-6'>
        <div class="box box-primary">
            <div class="box-body">

                <?= 'No info';
                /* GridView::widget([
                    'dataProvider' => $dataProvider1,
                    // 'filterModel' => $searchModel1,
                    'columns' => [
                        ['class' => 'kartik\grid\SerialColumn'],
                        [
                            'attribute' => 'id_service',
                            'format' => 'raw',
                            'value' => function ($model, $key, $index, $widget) {
                                return Html::a($model->id_service, Url::to(['services/view', 'id' => $model->id_service]), ['title' => 'View details']);
                            },
                        ],

                        [
                            'attribute' => 'id_type',
                            'value' => 'type.service_name',
                            'filterType' => GridView::FILTER_SELECT2,
                            'filter' => \app\models\ServiceTypes::getSelect(),
                            'filterWidgetOptions' => [
                                'pluginOptions' => ['allowClear' => true],
                            ],
                            'filterInputOptions' => ['placeholder' => 'All'],
                        ],

                        'plate_number',

                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model, $key, $index, $column) {
                                return $model->getStatus();
                            },

                        ],
                    ],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'toolbar' => [
                        [
                            'content' =>
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', Url::current(), ['class' => 'btn btn-default', 'title' => 'Refresh']),
                        ],
                        '{export}',
                        '{toggleData}',
                    ],
                    'export' => [
                        'fontAwesome' => true,
                    ],
                    'striped' => true,
                    'responsive' => true,
                    'hover' => true,
                    'showPageSummary' => false,
                    'panel' => [
                        'type' => GridView::TYPE_DEFAULT,
                        'heading' => 'Today\'s services',
                    ],
                    'persistResize' => true,
                    'toggleDataOptions' => ['minCount' => 10],
                    'itemLabelSingle' => 'actuación',
                    'itemLabelPlural' => 'actuaciones'
                ]);*/
                ?>

            </div>
        </div>
    </div>
</div>

<div class="box box-primary">
    <div class="box-body">
        <div class="site-index">

            <div class="row text-center">
                <p class="lead"><?= Html::img('@web/img/logo1.png', ['height' => '110']) ?></p>
                <!--<p class="lead "><?= yii::$app->params['appName'] ?></p>-->

            </div>


        </div>
    </div>
</div>