<?php

use app\models\Charts;
/* @var $this yii\web\View */
use app\models\Services;
use app\models\Users;
use dosamigos\chartjs\ChartJs;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::$app->name;
$class = '6';
?>
<div class="row">
    <?php
    if (Users::validateRol(Users::ROL_ADMIN)) {
        $class = '4';
    ?>


        <div class="col-md-<?= $class ?> col-sm-12 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?= Users::getIndexCount() ?></h3>

                    <p>Users</p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="<?= Url::to('users/index') ?>" class="small-box-footer">Ver todos <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    <?php
    }
    ?>

    <!-- ./col -->
    <div class="col-md-<?= $class ?> col-sm-12 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3><?= '1' ?></h3>

                <p>Services</p>
            </div>
            <div class="icon">
                <i class="fa fa-asterisk"></i>
            </div>
            <a href="<?= Url::to('services/index') ?>" class="small-box-footer">Ver todos <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-md-<?= $class ?> col-sm-12 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>0</h3>

                <p>Item3</p>
            </div>
            <div class="icon">
                <i class="fa fa-image"></i>
            </div>
            <a href="#" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-md-<?= $class ?> col-sm-12 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>0</h3>

                <p>Item4</p>
            </div>
            <div class="icon">
                <i class="fa fa-image"></i>
            </div>
            <a href="#" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
</div>


<div class="row">

    <div class='col-xs-12 col-lg-7 '>
        <div class="box box-primary">
            <div class="box-header">
                Services requested in last week
            </div>
            <div class="box-body">

                <?=

                ChartJs::widget([
                    'type' => 'line',
                    'options' => [
                        'height' => 242,
                        'responsive' => true,
                        'label' => [
                            'display' => false,
                        ],
                    ],
                    'data' => [
                        'labels' => Charts::getLastWeek(),
                        'datasets' => [
                            [
                                'label' => 'Services',
                                'backgroundColor' => "rgba(99,85,242,0.7)",
                                'borderColor' => "#933FF1",
                                'pointBackgroundColor' => "#6355f2",
                                'pointBorderColor' => "#933FF1",
                                'pointHoverBackgroundColor' => "#933FF1",
                                'pointHoverBorderColor' => "#933FF1",
                                'data' => Charts::getLastWeekServices(),
                            ],
                        ],
                    ],
                ]);
                ?>


            </div>
        </div>
    </div>

    <div class='col-xs-12 col-lg-5'>
        <div class="box box-primary">
            <div class="box-body">

                <?='No info';
               /* GridView::widget([
                    'dataProvider' => $dataProvider1,
                    // 'filterModel' => $searchModel1,
                    'columns' => [
                        ['class' => 'kartik\grid\SerialColumn'],
                        [
                            'attribute' => 'id_service',
                            'format' => 'raw',
                            'value' => function ($model, $key, $index, $widget) {
                                return Html::a($model->id_service, Url::to(['services/view', 'id' => $model->id_service]), ['title' => 'View details']);
                            },
                        ],

                        [
                            'attribute' => 'id_type',
                            'value' => 'type.service_name',
                            'filterType' => GridView::FILTER_SELECT2,
                            'filter' => \app\models\ServiceTypes::getSelect(),
                            'filterWidgetOptions' => [
                                'pluginOptions' => ['allowClear' => true],
                            ],
                            'filterInputOptions' => ['placeholder' => 'All'],
                        ],

                        'plate_number',

                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model, $key, $index, $column) {
                                return $model->getStatus();
                            },

                        ],
                    ],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'toolbar' => [
                        [
                            'content' =>
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', Url::current(), ['class' => 'btn btn-default', 'title' => 'Refresh']),
                        ],
                        '{export}',
                        '{toggleData}',
                    ],
                    'export' => [
                        'fontAwesome' => true,
                    ],
                    'striped' => true,
                    'responsive' => true,
                    'hover' => true,
                    'showPageSummary' => false,
                    'panel' => [
                        'type' => GridView::TYPE_DEFAULT,
                        'heading' => 'Today\'s services',
                    ],
                    'persistResize' => true,
                    'toggleDataOptions' => ['minCount' => 10],
                    'itemLabelSingle' => 'actuación',
                    'itemLabelPlural' => 'actuaciones'
                ]);*/
                ?>

            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body">
        <div class="site-index">

            <div class="row text-center">
                <p class="lead"><?= Html::img('@web/img/logo1.png', ['height' => '110']) ?></p>
                <p class="lead "><?=yii::$app->params['appName']?></p>
                
            </div>


        </div>
    </div>
</div>