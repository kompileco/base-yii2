<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
?>

<div class="login-box">
    <div class="login-logo">
        <span class='md'><?= Html::img('@web/img/logo2.png', ['height' => '60']) ?></span>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => true, 'type' => ActiveForm::TYPE_HORIZONTAL]); ?>

        <?= $form->field($model, 'email')->textInput() ?>

        <?= $form->field($model, 'password')->passwordInput() ?>


        <?=
        $form->field($model, 'rememberMe')->widget(SwitchInput::classname(), ['pluginOptions' => [
                //'handleWidth' => 60,
                'onText' => '<i class="glyphicon glyphicon-ok"></i>',
                'offText' => '<i class="glyphicon glyphicon-remove"></i>',
                'offColor' => 'danger',
                'onColor' => 'success',
                'handleWidth' => 20,
        ]]);
        ?>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <hr>
                <?= Html::submitButton('Iniciar sesión', ['class' => 'btn btn-flat', 'name' => 'login-button']) ?>
                <?= Html::resetButton('Reiniciar', ['class' => 'btn btn-default']) ?>
                <hr>
                <a href='<?= Url::to('remember') ?>'>Olvidé mi contraseña</a>
            </div>
        </div>



        <?php ActiveForm::end(); ?>

    </div>

</div>
