<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */
$trace=$exception->getTrace()[0];
$msg='File: '.$trace['file'].' --> ';
$msg.='Line: '.$trace['line'].' --> ';
$msg.='Function: '.$trace['function'].' --> ';
$msg.='Class: '.$trace['class'];

$this->title = $name;
?>
<section class="content">
    <div class="box box-primary">
        <div class="box-body">
            <div class="error-page">
                <h2 class="headline text-info"><i class="fa fa-warning text-yellow"></i></h2>

                <div class="error-content">
                    <h3><?= $name ?></h3>
                    <p>
                        <?= nl2br(Html::encode($message)) ?>
                    </p>
                    <p>
                        El anterior error se produjo al intentar procesar tu solicitud, 
                        puedes <a href='<?= Yii::$app->homeUrl ?>'>volver al inicio</a> 
                        o si crees que se trata de un error no dudes en 
                        comunicarte con nosotros a través del correo <a href='mailto:<?= Yii::$app->params['adminEmail'] ?>?subject=Error en MD: <?=($name.' - '.$message)?>&body=<?= ($msg)?>'><?= Yii::$app->params['adminEmail'] ?></a>

                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
