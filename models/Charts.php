<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Services;


class Charts extends Model
{
    const DAYS = 7;

    public static function getLastWeek($format = 'Y-m-d')
    {
        $start = strtotime("-" . static::DAYS . " days");

        for ($i = 0; $i <= static::DAYS; $i++) {
            if ($i == 0) {
                $day = date($format, $start);
            } else {
                $day = date($format, strtotime("+$i days", $start));
            }

            // if (date('w', strtotime($day))!=0&&date('w', strtotime($day))!=6&&!Yii::$app->Festivos->esFestivo(date('d', strtotime($day)), date('m', strtotime($day)))) {
            $days[] = $day;
            //}
        }
        //  dd($days);
        return $days;
    }

    public static function getLastWeekServices()
    {

        return 0;
    }
}
