<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RememberForm extends Model
{

    public $email;
    public $password;
    public $rememberMe = true;
    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email'], 'required'],
            [['email'], 'email', 'checkDNS' => true],
            [['email'], 'validateEmail'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user) {
                $this->addError($attribute, 'Usuario inexistente');
            }
        }
    }

    public function sendEmail()
    {
        /* @var $user User */
        $user = $this->getUser();
        if (!$user) {
            return false;
        }

        if (!Users::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                // d($user);dd($user->getErrors());
                return false;
            }
        }
        return Yii::$app->mailer
            ->compose(
                ['html' => 'recordarPassword'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['infoEmail'] => Yii::$app->params['infoName']])
            ->setTo($this->email)
            ->setSubject(Yii::$app->params['appName'] . ' - Cambiar contraseña')
            ->send();
    }

    public function sendEmailNew()
    {
        /* @var $user User */
        $user = $this->getUser();

        if (!$user) {
            return false;
        }

        if (!Users::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                // d($user);dd($user->getErrors());
                return false;
            }
        }
        return Yii::$app->mailer
            ->compose(
                ['html' => 'crearPassword'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['infoEmail'] => Yii::$app->params['infoName']])
            ->setTo($this->email)
            ->setSubject(Yii::$app->params['appName'] . ' - Crear contraseña')
            ->send();
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Users::findByUsername($this->email);
        }

        return $this->_user;
    }
}
