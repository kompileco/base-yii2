<?php

namespace app\models;

use Yii;
use yii\base\InvalidParamException;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ChangeForm extends Model {

    public $password;
    public $password_confirm;
    private $_user = false;
    private $token;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            // username and password are both required
            [['password', 'password_confirm'], 'required'],
            [['password', 'password_confirm'], 'string', 'length' => [6, 15]],
            // same as above but with explicitly specifying the attribute to compare with
            ['password_confirm', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    public function validateToken() {

        if (empty($this->token) || !is_string($this->token)) {
            return('El token no puede estar vacío');
        }
        $this->_user = Users::findByPasswordResetToken($this->token);
        if (!$this->_user) {
            return ('El token ha expirado');
        }
        return true;
    }

    public function __construct($token, $config = []) {
        $this->token = $token;
        parent::__construct($config);
    }

    public function attributeLabels() {
        return [
            'password' => 'Contraseña',
            'password_confirm' => 'Confirmar contraseña',
        ];
    }

    public function resetPassword() {
        $user = $this->_user;
        $user->setPassword($this->password);
        $user->removePasswordResetToken();
        return $user->save(false);
    }

}
