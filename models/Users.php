<?php

namespace app\models;

use Yii;
use yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use yii\base\Security;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @property string $email
 * @property int $rol
 * @property int $enabled
 * @property string $password
 * @property string $password_reset_token
 * @property string $auth_key
 */
class Users extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{

    const ESCENARIO_CREATE = 'create';
    const ESCENARIO_UPDATE = 'update';
    const ROL_USER = 2;
    const ROL_ADMIN = 1;

    public $admin;

    public $data;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'name',], 'required'],
            [['rol', 'enabled'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['enabled', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 45],
            [['email'], 'string', 'max' => 255],
            ['email', 'email', 'checkDNS' => true],
            [['password', 'password_reset_token'], 'string', 'max' => 65],
            [['auth_key'], 'string', 'max' => 32],
            [['email'], 'unique'],

        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nombre',
            'created_at' => 'Fecha de creación',
            'updated_at' => 'Fecha de actualización',
            'email' => 'Email',
            'rol' => 'Rol',
            'enabled' => 'Habilitado',
            'password' => 'Password',
            'password_reset_token' => 'Password Reset Token',
            'auth_key' => 'Auth Key',
        ];
    }

    public static function validateRol($rol, $usuario = null)
    {

        if (!$usuario) {
            $model = static::findOne(['email' => Yii::$app->user->identity->email, 'rol' => $rol]);
        } else {
            $model = static::findOne(['id' => $usuario, 'rol' => $rol]);
        }


        if ($model) {
            return true;
        } else {
            return false;
        }
    }

    public static function findByPasswordResetToken($token)
    {
        $expire = \Yii::$app->params['passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);

        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        //dd($token);
        return static::findOne([
            'password_reset_token' => $token
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        //dd($id);
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {

        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['email' => $username, 'enabled' => STATUS_ACTIVO]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        //return 41;
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {

        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {

        return $this->auth_key === $authKey;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = $this->generateRandomString();
        $this->save();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = $this->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {

        if (cryptPassword($password) == $this->password) {

            return true;
        } else {

            return false;
        }
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */


    private function generateRandomString($length = 32)
    {
        $bytes = Yii::$app->security->generateRandomKey($length);
        return substr(StringHelper::base64UrlEncode($bytes), 0, $length);
    }

    public function setPassword($password)
    {
        $this->password = cryptPassword($password);
    }


    public static function getIndexCount()
    {

        $n1 = self::find()->where(['enabled' => STATUS_ACTIVO])->count();
        $n2 = self::find()->count();

        return ($n1 . ' / ' . $n2);
    }
    static function getSelect()
    {
        $models = static::find()->select(['id_users', "CONCAT(identification_users,' - ',name,' ',surname,' - ',email) as data"])
            //->where(['habilitado' => STATUS_ACTIVO])
            ->orderBy(['id_users' => SORT_ASC])
            ->all();

        return ArrayHelper::map($models, 'id_users', 'data');
    }
}
